#BSA2019 - Second chance APP

[https://bitbucket.org/Maschin/bsa2019-second-chance-app]

#####Laravel - Vue - MySQL

####Installation

1 - Clone the repository to a local folder
```
git clone https://Maschin@bitbucket.org/Maschin/bsa2019-second-chance-app.git
```
2 - Enter to the the folder
```
cd bsa2019-second-chance-app
```
3 - Clone all submodules
```
git submodule update --init --recursive
```
4 - Enter the laradock folder and rename env-example to .env.
```
cd laradock
cp env-example .env
```

5 - Open .env file and set the following:
```
APP_CODE_PATH_HOST=../fc-groups-app
MARIADB_DATABASE=fc-groups-app
MARIADB_USER=homestead
```

6 - Run the containers
```
docker-compose up -d --build nginx php-fpm mariadb
```

7 - Enter the workspace container
```
docker-compose exec workspace bash
```

8 -  Install all dependencies via composer
```
composer install
```

9 - In the fc-groups-app project folder rename .env.example to .env
```
cp .env.example .env
```

10 - Open your project’s .env file and set the following:
```
DB_HOST=mariadb
DB_DATABASE=fc-groups-app
```

11 - Install NPM dependencies
```
yarn install
``` 

12 - Apply all DB migrations
```
php artisan migrate
```

13 - Generate Encryption Key
```
php artisan key:generate
```

14 - Let's build JavaScript
```
yarn watch
```

15 - Open your browser and visit localhost: http://localhost
```
That's it! enjoy :)
```

16 - To exit from app exit from workspace container and remove all containers
```
exit
docker-compose down -v    
```
 
##Endpoints:

#####Groups API
Get all groups
```
127.0.0.1/api/groups/
{method: 'GET'}
```

Get group by id
```
127.0.0.1/api/groups/{id}
{method: 'GET'}
``` 

Create group
```
127.0.0.1/api/groups
{method: 'POST'}
```

Delete group
```
127.0.0.1/api/groups/{id}
{method: 'DELETE'}
```
Generate matches in groups
~~~~
127.0.0.1/api/matches/generate/{id}
{method: 'POST'}
~~~~

#####Teams API
Get all teams
~~~~
127.0.0.1/api/teams
{method: 'GET'}
~~~~

Get team by id
~~~~
127.0.0.1/api/teams/{id}
{method: 'GET'}
~~~~

Create team
~~~~
127.0.0.1/api/teams?group_id={group_id}&name={Name of the team}
{method: 'POST'}
~~~~

Delete team
~~~~
127.0.0.1/api/teams/{id}
{method: 'DELETE'}
~~~~

#####Matches API
Get all matches
~~~~
127.0.0.1/api/matches
{method: 'GET'}
~~~~

Get all match by id
~~~~
127.0.0.1/api/matches/{id}
{method: 'GET'}
~~~~

Create match
~~~~
127.0.0.1/api/matches?group_id={group_id}&host_id={team1_id}&guest_id={team2_id}
{method: 'POST'}
~~~~

Update Match Score
~~~~
127.0.0.1/api/matches/{id}?host_score={score}&guest_score={score}
{method: 'PUT'}
~~~~
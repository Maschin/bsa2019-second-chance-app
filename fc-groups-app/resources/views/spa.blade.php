<?php
/**
 * Created by Michael Maschinoff
 * www.maschinoff.com
 * www.zeppit.pro
 * maschinov@zeppit.pro
 * Github: https://github.com/maschinoff
 * File: spa.blade.php
 * Date: 5/21/19
 * Time: 11:49 AM
 * Project: bsa2019-second-chance-app
 */ ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>FC Groups SPA - BSA2019</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <app></app>
    </div>
    <script src="{{ mix('js/bootstrap.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import App from './views/App'
import Home from './views/Home'
import GroupsIndex from './views/GroupsIndex'
import GroupsEdit from './views/GroupsEdit'
import NotFound from './views/NotFound'

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/groups',
            name: 'groups.index',
            component: GroupsIndex,
        },
        {
            path: '/groups/:id/edit',
            name: 'groups.edit',
            component: GroupsEdit
        },
        {
            path: '/404',
            name: '404',
            component: NotFound,
        },
        {
            path: '*',
            redirect: '/404'
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
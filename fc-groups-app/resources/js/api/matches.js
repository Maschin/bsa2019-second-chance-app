import axios from 'axios';

export default {
    all() {
        return axios.get('/api/matches');
    },
    find(id) {
        return axios.get(`/api/matches/${id}`);
    },
    update(id, data) {
        return axios.put(`/api/matches/${id}`, data);
    },
    generate(group_id) {
        return axios.post(`/api/matches/generate/${group_id}`)
    }
};
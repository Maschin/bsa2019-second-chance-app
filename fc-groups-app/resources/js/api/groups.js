import axios from 'axios';

export default {
    all() {
        return axios.get('/api/groups');
    },
    find(id) {
        return axios.get(`/api/groups/${id}`);
    },
    create() {
        return axios.post('/api/groups')
    },
    delete(id) {
        return axios.delete(`/api/groups/${id}`)
    }
};
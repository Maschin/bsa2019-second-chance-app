import axios from 'axios';

export default {
    all() {
        return axios.get('/api/teams');
    },
    find(id) {
        return axios.get(`/api/teams/${id}`);
    },
    create(data) {
        return axios.post('/api/teams', data)
    },
    delete(id) {
        return axios.delete(`/api/teams/${id}`)
    }
};
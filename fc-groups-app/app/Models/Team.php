<?php

namespace App\Models;

use App\Models\Match;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'group_id'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($team){
            $team->deleteMatches();
        });
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function hasMatch()
    {
        return Match::where('host_id', $this->id)
            ->whereNotNull('host_score')
            ->orWhere('guest_id', $this->id)
            ->whereNotNull('guest_score')
             ->count();
    }

    public function deleteMatches()
    {
        return Match::where('host_id', $this->id)
            ->orWhere('guest_id', $this->id)
            ->delete();
    }
}

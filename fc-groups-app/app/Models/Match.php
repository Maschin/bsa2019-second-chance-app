<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = ['group_id', 'host_id', 'host_score', 'guest_id', 'guest_score'];

    public function hostTeam()
    {
        return $this->belongsTo('App\Models\Team', 'host_id');
    }

    public function guestTeam()
    {
        return $this->belongsTo('App\Models\Team','guest_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }
}

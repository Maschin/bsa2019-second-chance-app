<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name'];

    public function teams()
    {
        return $this->hasMany('App\Models\Team');
    }

    public function matches()
    {
        return $this->hasMany('App\Models\Match');
    }

    private static function allowedNames()
    {
        return collect(range('a','z'));
    }

    private static function getUsedNames()
    {
        return self::all('name')->pluck('name');
    }

    public static function getFreeName()
    {
        $allowed = self::allowedNames();
        $used = self::getUsedNames();

        return $allowed->diff($used)->first();
    }
}

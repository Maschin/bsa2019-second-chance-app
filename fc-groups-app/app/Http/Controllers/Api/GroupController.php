<?php

namespace App\Http\Controllers\Api;

use App\Models\Group;
use App\Http\Resources\GroupResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function index()
    {
        return GroupResource::collection(Group::with('teams')->get());
    }

    public function show(Group $group)
    {
        return new GroupResource($group->load('teams', 'matches'));
    }

    public function store(Group $group, Request $request)
    {
        try {
            $data = ['name' => Group::getFreeName()];

            if ($data['name'] === null) {
                throw new \Exception('Could not create group');
            }

            return new GroupResource(Group::create($data));
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function destroy(Group $group)
    {
        try {
            DB::beginTransaction();
            if ($group->matches()->count()) {
                throw new \Exception('Can not delete group with matches');
            } else {
                $group->teams()->delete();

                if ($group->delete()) {
                    DB::commit();
                    return response(null, 204);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            abort(500, $e->getMessage());
        }
    }
}
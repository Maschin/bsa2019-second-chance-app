<?php

namespace App\Http\Controllers\Api;

use App\Models\Match;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\MatchResource;
use App\Http\Controllers\Controller;

class MatchController extends Controller
{
    public function index()
    {
        return MatchResource::collection(Match::with('group', 'hostTeam', 'guestTeam')->get());
    }

    public function show(Match $match)
    {
        return new MatchResource($match->load('group', 'hostTeam', 'guestTeam'));
    }

    //TODO switch to resources
    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'group_id' => 'required',
                'host_id' => 'required',
                'guest_id' => 'required'
            ]);

            $match = Match::where([
                ['host_id', '=', $data['host_id']],
                ['guest_id', '=', $data['guest_id']]
            ])->count();

            if (!$match) {
                return new MatchResource(Match::create($data));
            }
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function update(Match $match, Request $request)
    {
        $data = $request->validate([
            'host_score' => 'nullable|integer',
            'guest_score' => 'nullable|integer',
        ]);

        $match->update($data);

        return new MatchResource($match);
    }

    public function generate(Group $group)
    {
        try {
            $teams = $group->teams()->pluck('id')->toArray();
            $matches = self::seedGames($teams);
            $message = $this->handleMatchSaving($matches, $group);
            $response = ['data' => ['message' => $message]];
            return response()->json($response, 200);
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    private function handleMatchSaving($matches, $group)
    {
        DB::beginTransaction();

        try {
            $new = 0;

            foreach ($matches as $match) {
                $request = new Request();
                $request->setMethod('POST');
                $request->query->add([
                    'group_id' => $group->id,
                    'host_id' => $match[0],
                    'guest_id' => $match[1]
                ]);

                if ($this->store($request)) {
                    $new++;
                };
            }

            DB::commit();
            return 'The '.$new.' new matches created';
        }
        catch (\Exception $e) {
            DB::rollback();
            abort(500, $e->getMessage());
        }
    }

    private static function seedGames($teams)
    {
        $games = [];

        foreach ($teams as $team) {
            for ($i=1; $i < count($teams); $i++) {
                $games[] = [$team, $teams[$i]];
            }
            array_shift($teams);
        }

        return $games;
    }
}
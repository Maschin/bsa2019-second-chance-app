<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreTeamRequest;
use App\Models\Team;
use App\Http\Resources\TeamResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        return TeamResource::collection(Team::all());
    }

    public function show(Team $team)
    {
        return new TeamResource($team->load('group'));
    }

    public function store(Team $team, StoreTeamRequest $request)
    {
        try {
            $data = $request->validate([
                'name' => 'required',
                'group_id' => 'required|integer',
            ]);

            $team = Team::where([
                ['name', '=', $request['name']],
                ['group_id', '=', $request['group_id']]
            ])->count();

            if ($team) {
                throw new \Exception('The team '.$data['name'].' already exist in this group!');
            } else {
                return new TeamResource(Team::create($data));
            }
        } catch(\Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    public function update(Team $team, Request $request)
    {
        $data = $request->validate([
            'name' => 'required|unique:name,group_id',
            'group_id' => 'required|integer',
        ]);

        $team->update($data);

        return new TeamResource($team);
    }

    public function destroy(Team $team)
    {
        try {
            DB::beginTransaction();
            $matches = $team->hasMatch();

            if ( $matches ) {
                throw new \Exception('Unsuccessful: The ' . $team->name . ' has ' . $matches . ' played matches.');
            } else {
                $team->delete();
                DB::commit();
                return response( null, 204 );
            }
        } catch (\Exception $e) {
            DB::rollBack();
            abort(400, $e->getMessage());
        }
    }
}
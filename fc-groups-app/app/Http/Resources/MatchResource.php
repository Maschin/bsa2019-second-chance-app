<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'group' => $this->group->name,
            'host' => $this->hostTeam->name,
            'host_score' => $this->host_score,
            'guest' => $this->guestTeam->name,
            'guest_score' => $this->guest_score
        ];
    }
}

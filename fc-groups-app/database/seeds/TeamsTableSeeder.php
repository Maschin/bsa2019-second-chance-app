<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Team::class, 4)->create([
            'group_id' => $this->getRandomGroupId()
        ]);
    }

    private function getRandomGroupId()
    {
        $group = \App\Group::inRandomOrder()->first();
        return $group->id;
    }
}
